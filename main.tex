\documentclass[11pt, a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{eurosym}
\usepackage[hidelinks]{hyperref}
\usepackage[margin=3cm]{geometry}
\widowpenalty=10000
\clubpenalty=10000


\usepackage[colorinlistoftodos,prependcaption,textsize=scriptsize]{todonotes}

\begin{document}


\title{Open Software Landscape for Science Hardware Development in 2022}
\author{See contributors.txt for now}

\maketitle

\begin{abstract}
TODO
\end{abstract}


\section{Introduction}
Scientific research relies on scientists being able to replicate and build upon the works of others. Over the recent years there has been a push to increase the replicability of science. Rather than only publishing a paper about their work, scientists are encouraged or even required to openly share the raw data they have collected and the software used to generate and analyse the data. Scientific instruments are essential for experimental science, and their design and improvement drives new discoveries. The design of a new instrument is arguably more important to replicability in science than the raw data it produces. Complete, well-documented, open instrument designs allow scientists across the globe to inspect instruments' inner workings, replicate experiments with precision, to collaborate remotely on cutting edge instrumentation, and adapt the design for their own needs.

Open instrument designs technically do not require open source software. However, using proprietary software can often limit the audience that are able to inspect, adapt, and replicate a design. Even software provided at no-cost can limit participation. For example, academic licenses exclude community scientists. Cloud services can leave a project vulnerable, as it is not possible to permanent archive of work when it relies on a platform that, unlike software, cannot be archived for posterity.

The decision of whether to use open source tools for open instrumentation requires a level of pragmatism. If no open source alternative is available that replicates the features of proprietary tools, researchers rarely have the time, expertise, ability, or funding to write this software themselves. For greater replicability of science, funders should invest in the open source software toolchain needed to ensure that designs of scientific instruments can be transferred freely and can be archived in an editable and open standard format.

This manuscript seeks to describe the current state of the open source software landscape for scientific instrument design. It identifies strengths and weaknesses of current software, gaps where no software is available, young projects that are addressing gaps, and workarounds that can be used to keep an instrument design as open as possible. This software landscape is rapidly advancing. As such, this paper will remain and open repository to be updated periodically.

The scope of this paper covers all software that is required to design and build an instrument or experiment. The software needed to use the instrument or experiment is also in scope, including embedded software, instrument communication, and frameworks for instrument control software. To limit the scope to a manageable document we not consider instrument control programs for specific instruments, data analysis software, or lower level software for the any computers that are used to control instruments (such as the operating systems).

This manuscript considers both the design of novel instrumentation and also experimental setups for experiments that combine multiple standard instruments. To avoid the lengthy explanations we refer to both situations as ``instrument design'' for brevity.

\section{Overview of Software Landscape for Scientific Hardware Design}

\subsection{Mechanical CAD}

Often the key software in instrument design is a mechanical computer aided design (CAD) package. CAD packages can be used to design custom mechanical components. From these designs of custom components either technical drawings can be created to allow a machinist to produce the component, or via a CAM (computer aided machining) package G-Code can be created for either computer numerical control (CNC) machining or additive manufacturing of the component.

Mechanical CAD is also widely used for instruments without custom components. CAD models are widely available for most standard mechanical components, with many companies providing CAD representations of specialised components. This allows mechanical CAD to be used to design assemblies that consist of either available or custom components or often a mix of both.

Whist the use of CAD is almost ubiquitous for mechanical design and very common for the design of academic instrumentation, the field of CAD is itself frustratingly fractured. The most common format for sharing CAD designs, the STEP file (ISO 10303), is both so complex in definition that almost no CAD packages implement the standard in full, but also contains only the final geometry removing the parametric data that is essential for understanding and modifying a design. Each CAD package, whether open source or proprietary maintains their own file format. The process of exporting to transfer standards such as STEP, and importing the data into another (or even the same) CAD package results in data loss. As such when working on a design the files must be kept in a CAD packages native format, this limits collaboration as for effective collaboration all parties must be content to work in the same package.

For openly sharing an instrument design, native files are the only way to allow someone to inspect the design process rather than the final shape that is produced. Using open source CAD software allows anyone to open the native CAD file, but locks them into a specific package. Native CAD files from proprietary packages limit inspection of the design to those with access to the package. As such for sharing instrument design it is essential that both native formats and transfer formats such as STEP are provided. Some proprietary CAD software is offered at no-cost, however unless the package can be archived and run offline with no DRM or license keys, using these packages runs the risk of bait-and-switch tactics where cost is either introduced, or features become paid only. In the following sub-sections we will discuss open source CAD packages, their features and their support for transfer standards.

We note that in addition to STEP, STL is a common transfer standard. STL, whilst simpler than STEP, results in further data loss as it only includes a triangular mesh estimation of the final object. As such this estimation removes the ability to accurately specify curved surfaces, and numerical precision limits the ability to definitively define single planes of an object. As such an STL file (unlike a STEP file) cannot be used to create technical drawings, nor used for most traditional CNC machining. However it is the most common format used for additive manufacturing.


\subsubsection{Graphical CAD}
\begin{itemize}
 \item FreeCAD
 \begin{itemize}
  \item Very complete sketcher/part design, but toponaming issues remain
  \item Fast improving CAM/FEM features
  \item Technical drawings fast improving but still lack essentials such as hole callouts
  \item No consensus on assemblies (A2Plus, Assembly 3, Assembly 4) - Biggest issue for instrumentation. Are designs future proof?
 \end{itemize}
 \item Solvespace (does anyone have experience with solvespace)
\end{itemize}

\subsubsection{Programmatic CAD}

Another approach to generating digital 3D models for use in additive manufacturing or CNC milling is the use of programmatic CAD like BRL-CAD, CadQuery, ImplicitCAD, JSCAD, OpenSCAD, pythonOCC. % in alphabetic order
Rather than draw objects as is done in conventional CAD, these types of open source programs enable users to programmatically generate 3D objects. Built from geometric primitives, the objects are either described in their own domain-specific language (BRL-CAD, ImplicitCAD, OpenSCAD) or standard languages like Python (CadQuery, pythonOCC) or JavaScript (JSCAD).

OpenSCAD is a powerful parametric script-based CAD package that can be used for both research \cite{Pearce_2013, Baden_Chagas_Gage_Marzullo_Prieto-Godino_Euler_2015}, and STEM education \cite{Chacon_Saenz_de_la_Torre_Sanchez_2018, Gonzalez-Gomez_Valero-Gomez_Prieto-Moreno_Abderrahim_2012, Chacon_Saenz_Torre_Diaz_Esquembre_2017}. The parametric nature means that when a design is made all future problems in that set can be solved by users easily customizing the design. OpenSCAD has been recommended as a core tool for general open hardware design because it can be integrated with functionalities that depend on geometry (e.g. electrical resistance) \cite{Oberloier_Pearce_2018}. For example, OpenSCAD has been used to design everything from photometric systems for enzymatic nitrate quantification \cite{Wijnen_Petersen_Hunt_Pearce_2016} to open source LED controllers for arbitrary spectrum visual stimulation and optogenetics during 2-photon imaging \cite{Zimmermann_Maia_Chagas_Bartel_Pop_Prieto-Godino_Baden_2020}. OpenSCAD has also been used with the exploration of zebrafish to make a open-source platform for fluorescence microscopy, optogenetics, and accurate temperature control during behaviour of zebrafish \cite{Chagas_Prieto-Godino_Arrenberg_Baden_2017} as well as orientation tools for automated zebrafish screening assays \cite{Wittbrodt_Liebel_Gehrig_2014}.

Often scientists are relying on the geometry of an object to solve a problem that needs to be tested experimentally. A good example of this is the use of slot die processing of thin films. Slot dies have complicated internal geometries and are expensive to machine. By writing the design of the slot die in OpenSCAD many different internal geometries can be tested experimentally for 3D printing rapidly at low costs \cite{Beeker_Pringle_Pearce_2018}. Other examples of OpenSCAD being used for customizable design include mechanical optics equipment \cite{Zhang_Anzalone_Faria_Pearce_2013,Delmans_Haseloff_2018}, roller screws \cite{Guadagno_Loss_Pearce_2021}, random access plate storage \cite{Sanderson}, micromanipulators \cite{Hietanen_Heikkinen_Savin_Pearce_2018}, agrivoltaic testing systems \cite{Pearce_2021}, and portable microbiological mobile incubators and their associated attachments \cite{ Diep_Bizley_Ray_Edwards_2021}. OpenSCAD is also used extensively in microscopy \cite{Hohlbein_Diederich_Marsikova_Reynaud_Holden_Jahr_Haase_Prakash_2021}, with both the OpenFlexure microscope \cite{Stirling_Sanga_Nyakyi_Mwakajinga_Collins_Bumke_Knapper_Meng_McDermott_Bowman_2020, Stirling_Bumke_Collins_Dhokia_Bowman_2021} as well as large 3D microscopes \cite{Wijnen_Petersen_Hunt_Pearce_2016} relying on it.

The parametric nature of OpenSCAD has also been used to create an open source syringe pump library \cite{Wijnen_Hunt_Anzalone_Pearce_2014} as well as many scientific devices that use them like pH-stat \cite{Milanovic_Milanovic_Kragic_Kostic_2018}, wax-based rapid protyper for microfluidics \cite{Pearce_Anzalone_Heldt_2016}, open millifluidic \cite{LeSuer_Osgood_Stelnicki_Mendez_2018} and macroscale scientific fluid handling and automation \cite{Zhang_Wijnen_Pearce_2016}.

Medical hardware also benefits from the parametric nature of OpenSCAD by allowing quick customization of prosthetic, particularly of growing children \cite{De_Maria_Di_Pietro_Ravizza_Lantada_Ahluwalia_2020}, medical devices that can be distributed manufactured during the COVID-19 pandemic when supply chains were broken \cite{Pearce_2020} like ventilators \cite{Petsiuk_Tanikella_Dertinger_Pringle_Oberloier_Pearce_2020, Oberloier_Gallup_Pearce_2022}, pulse oximeters \cite{Metcalfe_Iravani_Graham-Harper-Cater_Bowman_Stirling_Wilson_2021} or nasopharyngeal swabs \cite{Gallup_Pringle_Oberloier_Tanikella_Pearce_2020}.
OpenSCAD has this parametric features built in and for best practices similar to coding it it recommended that designers list their variables at the front of their designs. For those less comfortable with code an open source 3D model customizer is available to integrate into websites \cite{Nilsiam_Pearce_2017}.

OpenSCAD is mesh based, which makes it less useful for technical drawings, but is extremely powerful for additive manufacturing. Creating assemblies of multiple parts is possible in OpenSCAD, but as there are no specific methods for this it requires direct calculation of component position and orientation which is challenging and generally time consuming. When designing parts for traditional workshops CAD software that uses a boundary representation (B-Rep) CAD kernel is preferred as B-Rep models can easily be converted into technical drawings or fed into CAM software for CNC machining. CadQuery is a python-based programmatic CAD that is built upon the OpenCascade B-Rep CAD kernel, and supports both constrained sketching and constrained assemblies. The graphical CAD package FreeCAD (as previously mentioned) can also be controlled via python scripts.

A study comparing OpenSCAD, an CadQuery, and FreeCAD Python scripts concluded that although Python for FreeCAD is more arduous to learn but has numerous advantages. \cite{Machado_Malpica_Borromeo_2019} These advantages include: i) being able to export to standard parametric models; ii) using Python language with its libraries; and iii) the ability to use and integrate the models in its graphical interface counterbalance the initial difficulties. However, for preparing students to being able to fabricate a complex 3D printable geometry for scientific hardware, a  programmatic approach is often preferred. For example \cite{West_Kuk_2016} argues, that between the GUI modeller Blender and the programmatic approach of OpenSCAD the latter one is recommended -- especially for people with programming background, including makers and hobbyists.

\subsubsection{CAM packages and slicers}

\subsection{Electronic CAD}

Many scientific instruments require various sensors and motion systems to enhance human perception and to automate measurements. These are typically controlled by electronic circuits, often computer controlled. Science hardware can embed existing functional modules (e.g. a USB camera, a computer board (Raspberry Pi, Arduino) or a humidity sensor module) and interconnect various such modules via hand-soldered or breadboard wires. However, in more complex cases it makes sense to develop an instrument based on electronic components like integrated circuits (chips) and to use printed circuit boards (PCB) to mount the electronic components and to interconnect them electrically. This improves reliability, reproducibility and reduces assembly time. Often a combination of modularized building blocks and a custom circuit board makes sense. Cost, availability of components, and manufacturability are key factors to consider during the instrument design.
In order to share and specify such designs, CAD systems are mandatory. 

In order to design the necessary printed circuit boards (PCB) open source software should be used. This extends the usefulnes of the designed instrument because the number of people who can adapt and re-use the design is maximized. Otherwise, people without access to a particular commercial product are excluded.
Proprietary tools often artifically limit the features - e.g. the size of the printed circuit board or the number of signal layers of the board - in their low-cost or free version, and a full version can be quite expensive. For example, in the OpenEEG project the PCB had to be split in two smaller PCBs (analog and digital board) in order to use the at that time free version of the commecrial EAGLE PCB design software, which limited the PCB size. Fortunately, there are now excellent open source solutions.

\subsubsection{Project representations and documentation}

There are multiple representations and documentations of an electronic design and ideally the CAD covers all of them: 
\begin{itemize}
	\item schematics: diagram which shows the electronic components as symbols and their connections 
	\item pcb: position of electronic components and traces, often on multiple copper layers, which connect them. 
	\item 3d mechanical model: a view of the final board which can be exported as a 3d model and imported into mechanical CAD systems.
	\item BOM: the bill of material specifies the components (part number, manufacturer, tolerances, part values, part package details etc.)
\end{itemize}

The design has to be fully documented including all component specifications  - which is called a bill of material (BOM) - and their interconnections. 


\subsubsection{Libraries of components}

There are millions of electronic components and it is almost impossible to make all of them available in a CAD system. However, it is very useful if as many parts as possible are part of the CAD package. This speeds up the design process because adding custom parts is time intensive and requires additional skills and a deeper understanding. 


\subsubsection{Fritzing}
Fritzing is a tool targeted at makers and hobbyists which excells in the combination of modules with small custom printed circuit boards. It is specifically designed to lower the entry barrier [Cite - \url{https://fritzing.org/media/uploads/publications/FritzingInfoBrochure-highRes.pdf}. 
In addition to schematics and PCB design it has also a unique wiring view as shown in Figure ~\ref{fig:Fritzing}. It offers a good way to get started in electronics and modularized instrument design.
The project is open source with GNU GPL v3 license and available from Github [Cite - \url{https://github.com/fritzing/fritzing-app/}].

\begin{figure}
	\includegraphics[width=8cm]{figures/Stepper_Motor_bb.png}
	\includegraphics[width=8cm]{figures/Stepper_Motor_schem.png}
	\includegraphics[width=8cm]{figures/Stepper_Motor_pcb.png}
	\caption{Breadboard, schematics and PCB view of the Fritzing software showing the same stepper motor driver example. The PCB is designed so that it can be plugged in a Arduino UNO microcontroller board.}
	\label{fig:Fritzing}
\end{figure}


\subsubsection{KiCAD}
KiCAD is probably the most advanced open source solution for PCB design. It includes tools for schematic design, printed circuit layout, library design and also ready to use libraries of parts and packages. It is a complex software package and targeted at more professional users. Its modularity can be used - together with Python scripting - to automate tasks and to develop plugins.
For example, there is a plugin which distributes components in a grid


\begin{figure}
	\includegraphics[width=8cm]{figures/kicad_start.png}
	\caption{KiCAD has a collection of tools to create schematics, PCBs and libraries. It can also be used to inspect PCB production gerber files.}
	\label{fig:Kicad1}
\end{figure}




\begin{figure}
	\includegraphics[width=8cm]{figures/kicad_pcb.png}
	\includegraphics[width=8cm]{figures/kicad_3d.png}
	\caption{KiCAD pcb design and the same as a 3d View.}
	\label{fig:Kicad2}
\end{figure}


The 3d mechanical model can also be imported into Freecad with a plugin called Kicad StepUp [Cite - \url{https://wiki.freecadweb.org/KicadStepUp_Workbench/it}] and then exported in various formats or directly used inside Freecad. 

\subsubsection{LibrePCB}

[Cite - \url{https://librepcb.org/}]


\subsection{Documentation}

Topics to cover in prose
\begin{itemize}
 \item Types of documentation needed
 \item Word processed/typeset documentation (Libre office, \LaTeX)
 \item Markup documentation, and converstion to multiple formats (Markdown, PanDoc, Jekyll, mdBook)
 \item Hardware specific documentation projects (GitBuilding)
\end{itemize}

% Section kick-off, see: https://gitlab.com/bath_open_instrumentation_group/open-software-landscape-for-science-hardware-development/-/issues/2

% Oral tradition is perhaps one of the earliest types of documentation, and is still widely used to pass on experiences beyond standard protocols and manuals, in both industry and science.

% This mechanism, however, has a great disadvantage: it's means are usually personal, synchronous, low-throughput interactions, which makes it hard to share and build upon.
% Furthermore, is most useful only when domain knowledge is already shared, in which undocumented contex provides most of the "common sense" required to correctly interpret the (usually brief) pieces of information.

% Without good documentation systems, \emph{reproducing} an open source hardware project is a hard task. Reasons for this include reinventing the wheel, repeating yourself.

% Alternatively to the above text, I propose a more down-to-earth and less blablabla explanation of the importance of documenting projects well.
Documentation of knowledge and experiences is essential to \emph{open} hardware projects.
Without it, reproducing or modifying them will also involve reliving many difficulties,
already faced and solved by the original developer.

Insufficient documentation translates directly to a waste of resources, both human and material,
which might be substantial, and even demoralizing.
This is specially critical for self-motivated makers with scarse resources; conditions which often lead individuals to choose open projects over proprietary alternatives.

The ideal means of documentation should be persistent, internationalized, accesible, free (as in freedom), \emph{complete} (see section: \hyperref[doctypes]{documentation types}), able to grow additivley and discussed openly.  % additivley: to accept contributions, "improve" through time or be updated or marked obsolete.
It should also \emph{not} rely on synchronous interactions, or be exclusively distributed by centralized, proprietary services. % Meaning it should be robust to temporary or permanent "absence" of the documenter and of the publishing service.

However, completeness in documentation always implies a compromise: how thoroughly should a project be documented? At the extremes, both insufficiency and excess lead to inefficent use of time.
However, we can reasonably expect that \emph{better} documentation includes or links to fundamental resources, material and theoretical, required to: use, repair, make, modify, contribute to, and understand the project.
% For completeness, the documentation should also document itself, in the same ways as the rest of the project.

Conceptually the documentation should be kept specific enough, while attending to the diversity in its potential users' backgrounds, and opening doors to a wider audience with external learning resources.

% A similar compromise arises often in open hardware development, when pondering the effort required to make an open project using either open or proprietary components and tools.

\subsubsection{Types of documentation needed}\label{doctypes}

% See issues:
% https://gitlab.com/bath_open_instrumentation_group/open-software-landscape-for-science-hardware-development/-/issues/2

Here we identify a set of main types of documentation, important for open source hardware projects:

% TODO: Write introductory paragraph, and explanation of the items below.
% TODO: The following should probably be in table format.

\begin{itemize}
 \item Project overview
 \subitem Questions answered: What is the project about? What is its intented motivations, purpose, and function? Does it have any known limitations or bugs? Is there a project roadmap? What is the development state? (ongoing, stale, requires funding, abandoned) how is the project's information organized? How do I use this documentation? Where can each type of information be found? What legal licences apply to this project?
   \subitem Typical output: main README file or a Wiki's frontpage.

 \item Design rationale: 
   \subitem Questions answered: What design choices and compromises were made during development? What resources and functional compromises restricted the design to its current state?
   \subitem Typical output: text section in a part's description, conceptually stating design choices, compromises, and interaction restrictions that apply.

 \item Working principles:
   \subitem Questions answered: Which physical principles support the object's design and function, and of its parts and modules?
   \subitem Typical output: text section in a part's description, describing how and why it works, and linking to external educational resources relevant to the principles.

 \item Functional specification:
   \subitem Questions answered: What is the overall function of the product and its limitations? Is the product modular? Which are the modules? How do modules and parts interact? What specifications must each module or part fulfill?
   \subitem Typical output: datasheets, a text section in parts' and modules' descriptions, explaining each of their characteristics relevant to interaction with other parts, and overall function of the product.

 \item Assembly instructions
   \subitem Questions answered: How do I build the product(s) of this project? What variants are available?
   \subitem Typical output: complete description of assembly steps, parts, and of required knowledge, number of people, skills, time, and tools.

 \item Usage instructions
   \subitem Questions answered: How do I use the product(s) of this project? Does they require maintenance?
   \subitem Typical output: intended usage instructions, alternate usage modalities, main and alternate usage restrictions (related to design rationale, functional specification and working principle items), maintenance guide.

 \item Development and contributing
   \subitem Questions answered: Which are the development objectives? What are people currently working on? How do I contact them? What skills do you require for each objective? (active or not). What documentation is missing or incomplete?
   \subitem Typical output: a prioritized list of issues (possibly grouped by milestone), a version control system (git, svn, mercurial, etc.), a discussion channel (wiki, forum, gitter, IRC, etc.).
\end{itemize}

\subsection{Software interfaces for instrumentation}

Topics to cover in prose
\begin{itemize}
 \item Back end - communication between instrumentation and program modules
 \begin{itemize}
  \item Busses - GPIB (old and poorly supported on some platforms), RS232 (well supported, polling, old, slow), emulated serial over USB ("virtual COM port"), USBTMC (just works on Linux, need more input on Windows), Ethernet (Best supported in hardware/software, can be a nightmare at universities due to IT policies), Bluetooth, LoRa
  \item Working from the ground up. The problems of multithreading etc. Discuss that this is one thing proprietary options such as LabView handles well (until the project scales).
  \item EPICS - For large scale systems, overly complex for smaller ones.
  \item Discuss WoT framework and LabThings initiative
  \item ROS (Robot Operating System)
  \item MQTT
  \item (There must be others?)
 \end{itemize}
 \item Front end
 \begin{itemize}
  \item Discuss frameworks for GUI applications. QT, kivy, GTK, web browser (others?).
  \item Again the issue of multithreading are problematic. The programmer should not have to handle this explicitly to build a simple instrument.
  \item Look into EPICS front ends
  \item Discuss web-based options with client server interface (custom code, LabThings)
 \end{itemize}
\end{itemize}


\subsection{Collaboration and Project management}

Topics to cover in prose
\begin{itemize}
 \item Collaboration on design
 \begin{itemize}
  \item Git - pros and cons for design
   \begin{itemize}
    \item Pro - For simpler text based formats, can be a great way to keep track of changes and who made them. Not all text based formats work well with git however, an example being XML.
    \item Con - Often confusing for non-coders, things like merge conflicts with hardware files can be terribly frustrating.
    \item Con - Tooling is (overall) geared towards programmers, not designers and engineers.
    \item Con - Not good with binary files often used and exported by CAD software.
   \end{itemize}
  \item Git based collaboration platforms (GitLab, Gitea), also proprietary options such as GitHub [Cite - \url{https://www.techrxiv.org/articles/preprint/HardOps_Utilising_the_software_development_toolchain_for_hardware_design/15052848}]
  \item Cloud storage, has issues with tracking changes effectively, and on open collaboration (NextCloud, OwnCloud, -many proprietary options)
  \item Proprietary platforms like WikiFactory and GrabCAD offer CAD tools for online-collaboration and offer options for manufacturing (open-source licenses can be selected by-design in WikiFactory). All history is stored within the platform causing possible lock-in. Open source collaborative CAD tools that provide a clear record of authorship are needed.
 \end{itemize}
 \item Project management
 \begin{itemize}
  \item Project management software becomes important only if a project grows beyond a few people in one place who maintain a ToDo list. Many ways to do project management, adopting the workflow is often the issue, not finding software
  \item GitLab (or other Git Platform) Issues. Beneficial for projects using these platforms as all in one place. GitLab has Epics, allowing Gantt charts, but this is a premium feature, available at no cost for projects on their open source program, but not available to any open repo.
  \item Open platforms for project management like Taiga and Redmine exist if needed.
 \end{itemize}
\end{itemize}
\subsection{Meta tools}

Topics to cover in prose
\begin{itemize}
 \item Open software development has numerous tools to assess quality of code, and health of projects. These can help a project avoid pitfalls. Similar tools are needed for hardware.
 \item Some meta projects in their infancy for OpenSCAD
 \begin{itemize}
  \item OpenSCAD Docsgen - Documentation of the code not the hardware
  \item SCA2D - Code linter for OpenSCAD
 \end{itemize}
 \item This is becoming more of a topic of discussion in the CodeCAD (programmatic CAD) community, with using things like continuous integration to check part tolerances, assembly fit, do FEA testing, etc. Example of a recent article that touches on this topic can be found [here \url{https://medium.com/embedded-ventures/mechanical-cad-yesterday-today-and-tomorrow-981cef7e06b1}].
\end{itemize}

\section{Discussion and Future needs}


Topics to cover in prose
\begin{itemize}
 \item Open Hardware is rarely named as key element in established Open Science definitions. Anchoring it as one equal to FOSS, Open Access, Open Educational Ressources (OER) etc. might help to foster attention on domain-specific applications and future support.
 \item Institutional/funding support is needed for software. Science relies on instrumentation, and replication of that instrumentation. Academic networks and cooperatives could start to fund OSH-related software on a regular basis instead of buying licenses of e.g. proprietary CAx-software on a large scale.
 \item CAD interoperability is a problem that goes well beyond science instrumentation. It is hard to see how a specific format taking off without buy in from proprietary options. However, what is their incentive when it reduces their ability to lock in? Science instrumentation is too small a market share for open access policies to effect change.
 \item Documentation is essential for replication but poorly rewarded. Open Access could require documentation, but documentation completeness is very hard to judge. Tools for automating documentation could improve replicability of scientific instrumentation. Furthermore, patents, papers and peer-reviews are often considered as criteria for research exemptions and grants. Universities, their networks and funding organizations could acknowledge contributions to OH-related FOSS, OSHWA certifications and peer-reviews according to DIN SPEC 3105 as equal to them.
 \item Software for hardware control is complex to write well. Well supported frameworks are needed for this. Libraries that add features are often preferable to frameworks as they add features rather than prescribe how code is written. This is however is not really possible for something as complex as abstracting away multithreading.
 \item In some areas, usability and features of the tools are way inferior to commercial proprietary alternatives. If institutionalized and funded, the Open Hardware community might want to focus on new features, that could incentivize users of conventional tools to switch, to gain attention. It might as well help to create communities of practice dedicated to improve overall aesthetics and user experience of FOSS tools, to broaden their influence to target larger – even semi-professional – audiences.
\end{itemize}

\section{Conclusion}

%Just picked a style at random, will change when we have a venue
\bibliographystyle{IEEEtran}
\bibliography{refs}
\end{document}
